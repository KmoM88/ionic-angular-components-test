import { Item } from './../interfaces/interfaces';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { delay } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getUsers () {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  getMenuItems() {
    return this.http.get<Item[]>('/assets/data/menu.json');
  }

  getAlbums () {
    return this.http.get<any>('https://jsonplaceholder.typicode.com/albums');
  }

  getSuperheroes() {
    return this.http.get<any>('/assets/data/superheroes.json')
            .pipe(
              delay(1500)
            );
  }
}
