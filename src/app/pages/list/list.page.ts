import { DataService } from './../../services/data.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { IonList, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

@ViewChild('list') list: IonList;

  users: Observable<any>;

  constructor(
    private dataService: DataService,
    private toastCtrl: ToastController
    ) { }

  ngOnInit() {
    this.users = this.dataService.getUsers();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  favorite(user) {
    this.presentToast('Guardado en favoritos');
    console.log('favorite',user);
    this.list.closeSlidingItems();
  }
  share(user) {
    this.presentToast('Usuario');
    console.log('share',user);
    this.list.closeSlidingItems();
  }
  unread(user) {
    this.presentToast('Borrado');
    console.log('borrar',user);
    this.list.closeSlidingItems();
  }
}
