import { DataService } from './../../services/data.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-segment',
  templateUrl: './segment.page.html',
  styleUrls: ['./segment.page.scss'],
})
export class SegmentPage implements OnInit {

  @ViewChild('segment') segment: IonSegment;

  superHeroes: Observable<any>;
  publisher = '';

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.superHeroes = this.dataService.getSuperheroes();
  }

  segmentChange(event) {
    const valorSegmento = event.detail.value;
    if (valorSegmento === 'Todos'){
      this.publisher = '';
      return;
    }
    this.publisher = valorSegmento;
  }

}
