import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.page.html',
  styleUrls: ['./slides.page.scss'],
})
export class SlidesPage implements OnInit {

  slides = [
    {
      img: 'assets/dinero.svg',
      titulo: 'Save money on <br>gym membership'
    },
    {
      img: 'assets/mujer.svg',
      titulo: 'Visible changes <br> in 3 weeks'
    },
    {
      img: 'assets/pastel.svg',
      titulo: 'Forget about <br>strict diet'
    }
  ]

  constructor() { }

  ngOnInit() {
  }


}
